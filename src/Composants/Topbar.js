import React from 'react';
import '../App.css';
import {Link} from "react-router-dom";

class Topbar extends React.Component {

    render() {
        return (
            <nav className="navbar navbar-dark bg-dark">
                <div className="container">
                    <Link to="/signup/" className="navbar-brand"> Se connecter </Link>
                    <Link to="/signin/" className="navbar-brand"> S'inscrire </Link>
                </div>
            </nav>
        );
    }
    
}

export default Topbar;
