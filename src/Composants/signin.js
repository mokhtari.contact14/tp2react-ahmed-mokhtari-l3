import React from 'react';
import '../App.css';
import {Form , Field} from "react-final-form"

class Signin extends React.Component {

    onSubmit(lk){
       if(lk.password === lk.ConfirmPassword){
        const url = 'https://los.ling.fr/user'
        const Fetch_Data = {
            method:'PUT',
            body:JSON.stringify(lk)
        }
        fetch(url,Fetch_Data)
            .then(SF=>{
                console.log(SF)
                document.querySelector('.signup').removeChild(document.querySelector('.myForm'));
                document.querySelector('.signup').removeChild(document.querySelector('.title'));
                const rpFinal = document.createElement();   
                rpFinal.innerHTML = "Votre compte a bien été crée !";                 
                document.querySelector('.signup').appendChild(rpFinal);
            })  
       }else{
            const noMatch = document.createElement("div");   
            noMatch.innerHTML = "Password do not match";                 
            document.querySelector('.myForm').appendChild(noMatch);
       }  
    }

    render(){
        return(
        <div className="signup container-fluid text-center">
            <h2 className="title">Inscription</h2>
            <Form
            onSubmit = {this.onSubmit}
            render = {({handleSubmit})=>(
                <form className="myForm" onSubmit={handleSubmit}>
                    <div>
                        <h3>Pseudo</h3>
                        <Field
                            name="pseudo"
                            component="input"
                            type="text"
                            required
                        />
                    </div>
                    <div>
                        <h3>Mot de passe</h3>
                        <Field
                            name="password"
                            component="input"
                            type="password"
                            required
                        />
                    </div>
                    <div>
                        <h3>Confirmer votre mot de passe</h3>
                        <Field
                            name="ConfirmPassword"
                            component="input"
                            type="password"
                            required
                        />
                    </div>
                    <button className="btn" type="submit">Valider</button>
                </form>
            )}/>
        </div>);
    }
}

export default Signin;