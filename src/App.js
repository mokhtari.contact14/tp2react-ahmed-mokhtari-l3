import React from 'react';
import './App.css';
import Nav from './Composants/Topbar';
import signin from './Composants/signin';
import signup from './Composants/signup';
import {BrowserRouter as Router , Route } from "react-router-dom";


class App extends React.Component {
  
  render() {
    return (
      <div className="App">
        <Router>
          <Nav />
          <Route path="/signin/" component={signin}/>
          <Route path="/signup/" component={signup}/>
        </Router>
      </div>
    );
  }
}

export default App;
